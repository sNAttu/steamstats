using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace HoyryStats.Migrations
{
    public partial class AllModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.CreateTable(
                name: "AvailableGameStats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AvailableGameStats", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "SteamPlayer",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    avatar = table.Column<string>(nullable: true),
                    avatarfull = table.Column<string>(nullable: true),
                    avatarmedium = table.Column<string>(nullable: true),
                    communityvisibilitystate = table.Column<int>(nullable: false),
                    gameCount = table.Column<int>(nullable: false),
                    lastlogoff = table.Column<string>(nullable: true),
                    loccityid = table.Column<int>(nullable: false),
                    loccountrycode = table.Column<string>(nullable: true),
                    locstatecode = table.Column<string>(nullable: true),
                    personastate = table.Column<int>(nullable: false),
                    personastateflags = table.Column<int>(nullable: false),
                    primaryclanid = table.Column<int>(nullable: false),
                    profilestate = table.Column<int>(nullable: false),
                    profileurl = table.Column<string>(nullable: true),
                    realname = table.Column<string>(nullable: true),
                    timecreated = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamPlayer", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "SteamGameAchievement",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AvailableGameStatsId = table.Column<int>(nullable: true),
                    defaultvalue = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    displayName = table.Column<string>(nullable: true),
                    @hidden = table.Column<int>(name: "hidden", nullable: false),
                    icon = table.Column<string>(nullable: true),
                    icongray = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamGameAchievement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SteamGameAchievement_AvailableGameStats_AvailableGameStatsId",
                        column: x => x.AvailableGameStatsId,
                        principalTable: "AvailableGameStats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "SteamGameDefaultStats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AvailableGameStatsId = table.Column<int>(nullable: true),
                    defaultvalue = table.Column<int>(nullable: false),
                    displayname = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamGameDefaultStats", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SteamGameDefaultStats_AvailableGameStats_AvailableGameStatsId",
                        column: x => x.AvailableGameStatsId,
                        principalTable: "AvailableGameStats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "SteamAchievement",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SteamPlayerId = table.Column<long>(nullable: true),
                    lastUpdated = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    percent = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamAchievement", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SteamAchievement_SteamPlayer_SteamPlayerId",
                        column: x => x.SteamPlayerId,
                        principalTable: "SteamPlayer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "SteamGame",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SteamPlayerId = table.Column<long>(nullable: true),
                    availableGameStatsId = table.Column<int>(nullable: true),
                    gameName = table.Column<string>(nullable: true),
                    gameVersion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamGame", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SteamGame_SteamPlayer_SteamPlayerId",
                        column: x => x.SteamPlayerId,
                        principalTable: "SteamPlayer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SteamGame_AvailableGameStats_availableGameStatsId",
                        column: x => x.availableGameStatsId,
                        principalTable: "AvailableGameStats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddColumn<DateTime>(
                name: "lastUpdated",
                table: "SteamNewsItem",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "lastUpdated", table: "SteamNewsItem");
            migrationBuilder.DropTable("SteamAchievement");
            migrationBuilder.DropTable("SteamGame");
            migrationBuilder.DropTable("SteamGameAchievement");
            migrationBuilder.DropTable("SteamGameDefaultStats");
            migrationBuilder.DropTable("SteamPlayer");
            migrationBuilder.DropTable("AvailableGameStats");
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
