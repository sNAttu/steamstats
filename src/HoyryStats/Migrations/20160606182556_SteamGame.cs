using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace HoyryStats.Migrations
{
    public partial class SteamGame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_PlayersGameModel_SteamPlayer_playerId", table: "PlayersGameModel");
            migrationBuilder.DropForeignKey(name: "FK_PlayerStats_SteamPlayer_steamID", table: "PlayerStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId", table: "SteamPlayerFriend");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.CreateTable(
                name: "AvailableGameStats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AvailableGameStats", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "SteamGame",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    availableGameStatsId = table.Column<int>(nullable: true),
                    gameName = table.Column<string>(nullable: true),
                    gameVersion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamGame", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SteamGame_AvailableGameStats_availableGameStatsId",
                        column: x => x.availableGameStatsId,
                        principalTable: "AvailableGameStats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "SteamGameAchievement",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AvailableGameStatsId = table.Column<int>(nullable: true),
                    defaultvalue = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    displayName = table.Column<string>(nullable: true),
                    @hidden = table.Column<int>(name: "hidden", nullable: false),
                    icon = table.Column<string>(nullable: true),
                    icongray = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamGameAchievement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SteamGameAchievement_AvailableGameStats_AvailableGameStatsId",
                        column: x => x.AvailableGameStatsId,
                        principalTable: "AvailableGameStats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "SteamGameDefaultStats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AvailableGameStatsId = table.Column<int>(nullable: true),
                    defaultvalue = table.Column<int>(nullable: false),
                    displayname = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamGameDefaultStats", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SteamGameDefaultStats_AvailableGameStats_AvailableGameStatsId",
                        column: x => x.AvailableGameStatsId,
                        principalTable: "AvailableGameStats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_PlayersGameModel_SteamPlayer_playerId",
                table: "PlayersGameModel",
                column: "playerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayerStats_SteamPlayer_steamID",
                table: "PlayerStats",
                column: "steamID",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId",
                table: "SteamPlayerFriend",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_PlayersGameModel_SteamPlayer_playerId", table: "PlayersGameModel");
            migrationBuilder.DropForeignKey(name: "FK_PlayerStats_SteamPlayer_steamID", table: "PlayerStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId", table: "SteamPlayerFriend");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropTable("SteamGame");
            migrationBuilder.DropTable("SteamGameAchievement");
            migrationBuilder.DropTable("SteamGameDefaultStats");
            migrationBuilder.DropTable("AvailableGameStats");
            migrationBuilder.AddForeignKey(
                name: "FK_PlayersGameModel_SteamPlayer_playerId",
                table: "PlayersGameModel",
                column: "playerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayerStats_SteamPlayer_steamID",
                table: "PlayerStats",
                column: "steamID",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId",
                table: "SteamPlayerFriend",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
