using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace HoyryStats.Migrations
{
    public partial class organizations4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_AvailableGameStats_SteamGame_relatedGameId", table: "AvailableGameStats");
            migrationBuilder.DropForeignKey(name: "FK_PlayersGameModel_SteamPlayer_playerId", table: "PlayersGameModel");
            migrationBuilder.DropForeignKey(name: "FK_PlayerStats_SteamPlayer_steamID", table: "PlayerStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamGameAchievement_AvailableGameStats_statId", table: "SteamGameAchievement");
            migrationBuilder.DropForeignKey(name: "FK_SteamGameDefaultStats_AvailableGameStats_statId", table: "SteamGameDefaultStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId", table: "SteamPlayerFriend");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.CreateTable(
                name: "Organization",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    isActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organization", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "SteamPlayerOrganization",
                columns: table => new
                {
                    OrganizationId = table.Column<int>(nullable: false),
                    SteamPlayerId = table.Column<int>(nullable: false),
                    SteamPlayerId1 = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamPlayerOrganization", x => new { x.OrganizationId, x.SteamPlayerId });
                    table.ForeignKey(
                        name: "FK_SteamPlayerOrganization_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SteamPlayerOrganization_SteamPlayer_SteamPlayerId1",
                        column: x => x.SteamPlayerId1,
                        principalTable: "SteamPlayer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_AvailableGameStats_SteamGame_relatedGameId",
                table: "AvailableGameStats",
                column: "relatedGameId",
                principalTable: "SteamGame",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayersGameModel_SteamPlayer_playerId",
                table: "PlayersGameModel",
                column: "playerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayerStats_SteamPlayer_steamID",
                table: "PlayerStats",
                column: "steamID",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamGameAchievement_AvailableGameStats_statId",
                table: "SteamGameAchievement",
                column: "statId",
                principalTable: "AvailableGameStats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamGameDefaultStats_AvailableGameStats_statId",
                table: "SteamGameDefaultStats",
                column: "statId",
                principalTable: "AvailableGameStats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId",
                table: "SteamPlayerFriend",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_AvailableGameStats_SteamGame_relatedGameId", table: "AvailableGameStats");
            migrationBuilder.DropForeignKey(name: "FK_PlayersGameModel_SteamPlayer_playerId", table: "PlayersGameModel");
            migrationBuilder.DropForeignKey(name: "FK_PlayerStats_SteamPlayer_steamID", table: "PlayerStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamGameAchievement_AvailableGameStats_statId", table: "SteamGameAchievement");
            migrationBuilder.DropForeignKey(name: "FK_SteamGameDefaultStats_AvailableGameStats_statId", table: "SteamGameDefaultStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId", table: "SteamPlayerFriend");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropTable("SteamPlayerOrganization");
            migrationBuilder.DropTable("Organization");
            migrationBuilder.AddForeignKey(
                name: "FK_AvailableGameStats_SteamGame_relatedGameId",
                table: "AvailableGameStats",
                column: "relatedGameId",
                principalTable: "SteamGame",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayersGameModel_SteamPlayer_playerId",
                table: "PlayersGameModel",
                column: "playerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayerStats_SteamPlayer_steamID",
                table: "PlayerStats",
                column: "steamID",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamGameAchievement_AvailableGameStats_statId",
                table: "SteamGameAchievement",
                column: "statId",
                principalTable: "AvailableGameStats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamGameDefaultStats_AvailableGameStats_statId",
                table: "SteamGameDefaultStats",
                column: "statId",
                principalTable: "AvailableGameStats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId",
                table: "SteamPlayerFriend",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
