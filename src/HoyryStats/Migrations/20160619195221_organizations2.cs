using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace HoyryStats.Migrations
{
    public partial class organizations2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_AvailableGameStats_SteamGame_relatedGameId", table: "AvailableGameStats");
            migrationBuilder.DropForeignKey(name: "FK_PlayersGameModel_SteamPlayer_playerId", table: "PlayersGameModel");
            migrationBuilder.DropForeignKey(name: "FK_PlayerStats_SteamPlayer_steamID", table: "PlayerStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamGameAchievement_AvailableGameStats_statId", table: "SteamGameAchievement");
            migrationBuilder.DropForeignKey(name: "FK_SteamGameDefaultStats_AvailableGameStats_statId", table: "SteamGameDefaultStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId", table: "SteamPlayerFriend");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.AddForeignKey(
                name: "FK_AvailableGameStats_SteamGame_relatedGameId",
                table: "AvailableGameStats",
                column: "relatedGameId",
                principalTable: "SteamGame",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayersGameModel_SteamPlayer_playerId",
                table: "PlayersGameModel",
                column: "playerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayerStats_SteamPlayer_steamID",
                table: "PlayerStats",
                column: "steamID",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamGameAchievement_AvailableGameStats_statId",
                table: "SteamGameAchievement",
                column: "statId",
                principalTable: "AvailableGameStats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamGameDefaultStats_AvailableGameStats_statId",
                table: "SteamGameDefaultStats",
                column: "statId",
                principalTable: "AvailableGameStats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId",
                table: "SteamPlayerFriend",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_AvailableGameStats_SteamGame_relatedGameId", table: "AvailableGameStats");
            migrationBuilder.DropForeignKey(name: "FK_PlayersGameModel_SteamPlayer_playerId", table: "PlayersGameModel");
            migrationBuilder.DropForeignKey(name: "FK_PlayerStats_SteamPlayer_steamID", table: "PlayerStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamGameAchievement_AvailableGameStats_statId", table: "SteamGameAchievement");
            migrationBuilder.DropForeignKey(name: "FK_SteamGameDefaultStats_AvailableGameStats_statId", table: "SteamGameDefaultStats");
            migrationBuilder.DropForeignKey(name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId", table: "SteamPlayerFriend");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.AddForeignKey(
                name: "FK_AvailableGameStats_SteamGame_relatedGameId",
                table: "AvailableGameStats",
                column: "relatedGameId",
                principalTable: "SteamGame",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayersGameModel_SteamPlayer_playerId",
                table: "PlayersGameModel",
                column: "playerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayerStats_SteamPlayer_steamID",
                table: "PlayerStats",
                column: "steamID",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamGameAchievement_AvailableGameStats_statId",
                table: "SteamGameAchievement",
                column: "statId",
                principalTable: "AvailableGameStats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamGameDefaultStats_AvailableGameStats_statId",
                table: "SteamGameDefaultStats",
                column: "statId",
                principalTable: "AvailableGameStats",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId",
                table: "SteamPlayerFriend",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
