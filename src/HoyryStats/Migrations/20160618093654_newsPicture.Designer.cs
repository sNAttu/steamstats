using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using HoyryStats.Models;

namespace HoyryStats.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160618093654_newsPicture")]
    partial class newsPicture
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("HoyryStats.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("HoyryStats.Models.AvailableGameStats", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("relatedGameId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("HoyryStats.Models.PlayersGameModel", b =>
                {
                    b.Property<long>("playerId");

                    b.Property<int>("game");

                    b.Property<int>("playTime");

                    b.HasKey("playerId", "game");
                });

            modelBuilder.Entity("HoyryStats.Models.PlayerStats", b =>
                {
                    b.Property<string>("gameName");

                    b.Property<long>("steamID");

                    b.Property<bool>("success");

                    b.HasKey("gameName", "steamID");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamAchievement", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("game");

                    b.Property<DateTime>("lastUpdated");

                    b.Property<string>("name");

                    b.Property<decimal>("percent");

                    b.HasKey("ID");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamGame", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("gameName");

                    b.Property<string>("gameVersion");

                    b.Property<long>("steamGameId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamGameAchievement", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("defaultvalue");

                    b.Property<string>("description");

                    b.Property<string>("displayName");

                    b.Property<int>("hidden");

                    b.Property<string>("icon");

                    b.Property<string>("icongray");

                    b.Property<string>("name");

                    b.Property<int>("statId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamGameDefaultStats", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("defaultvalue");

                    b.Property<string>("displayname");

                    b.Property<string>("name");

                    b.Property<int>("statId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamNewsItem", b =>
                {
                    b.Property<string>("ID");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("author");

                    b.Property<string>("contents");

                    b.Property<string>("date");

                    b.Property<string>("feedLabel");

                    b.Property<string>("feedname");

                    b.Property<bool>("isExternalUrl");

                    b.Property<DateTime>("lastUpdated");

                    b.Property<string>("title");

                    b.Property<string>("url");

                    b.HasKey("ID");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamPlayer", b =>
                {
                    b.Property<long>("Id");

                    b.Property<string>("avatar");

                    b.Property<string>("avatarfull");

                    b.Property<string>("avatarmedium");

                    b.Property<int>("communityvisibilitystate");

                    b.Property<int>("gameCount");

                    b.Property<string>("lastlogoff");

                    b.Property<int>("loccityid");

                    b.Property<string>("loccountrycode");

                    b.Property<string>("locstatecode");

                    b.Property<int>("personastate");

                    b.Property<int>("personastateflags");

                    b.Property<string>("primaryclanid");

                    b.Property<int>("profilestate");

                    b.Property<string>("profileurl");

                    b.Property<string>("realname");

                    b.Property<string>("timecreated");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamPlayerAchievements", b =>
                {
                    b.Property<string>("apiname");

                    b.Property<string>("PlayerStatsgameName");

                    b.Property<long?>("PlayerStatssteamID");

                    b.Property<int>("achieved");

                    b.HasKey("apiname");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamPlayerFriend", b =>
                {
                    b.Property<long>("Id");

                    b.Property<long>("SteamPlayerId");

                    b.Property<string>("friendsSince");

                    b.Property<string>("relationship");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasAnnotation("Relational:TableName", "AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasAnnotation("Relational:TableName", "AspNetUserRoles");
                });

            modelBuilder.Entity("HoyryStats.Models.AvailableGameStats", b =>
                {
                    b.HasOne("HoyryStats.Models.SteamGame")
                        .WithOne()
                        .HasForeignKey("HoyryStats.Models.AvailableGameStats", "relatedGameId");
                });

            modelBuilder.Entity("HoyryStats.Models.PlayersGameModel", b =>
                {
                    b.HasOne("HoyryStats.Models.SteamPlayer")
                        .WithMany()
                        .HasForeignKey("playerId");
                });

            modelBuilder.Entity("HoyryStats.Models.PlayerStats", b =>
                {
                    b.HasOne("HoyryStats.Models.SteamPlayer")
                        .WithOne()
                        .HasForeignKey("HoyryStats.Models.PlayerStats", "steamID");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamGameAchievement", b =>
                {
                    b.HasOne("HoyryStats.Models.AvailableGameStats")
                        .WithMany()
                        .HasForeignKey("statId");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamGameDefaultStats", b =>
                {
                    b.HasOne("HoyryStats.Models.AvailableGameStats")
                        .WithMany()
                        .HasForeignKey("statId");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamPlayerAchievements", b =>
                {
                    b.HasOne("HoyryStats.Models.PlayerStats")
                        .WithMany()
                        .HasForeignKey("PlayerStatsgameName", "PlayerStatssteamID");
                });

            modelBuilder.Entity("HoyryStats.Models.SteamPlayerFriend", b =>
                {
                    b.HasOne("HoyryStats.Models.SteamPlayer")
                        .WithMany()
                        .HasForeignKey("SteamPlayerId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("HoyryStats.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("HoyryStats.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("HoyryStats.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });
        }
    }
}
