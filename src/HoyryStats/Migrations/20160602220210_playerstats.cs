using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace HoyryStats.Migrations
{
    public partial class playerstats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_PlayersGameModel_SteamPlayer_playerId", table: "PlayersGameModel");
            migrationBuilder.DropForeignKey(name: "FK_SteamAchievement_SteamPlayer_SteamPlayerId", table: "SteamAchievement");
            migrationBuilder.DropForeignKey(name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId", table: "SteamPlayerFriend");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "SteamPlayerId", table: "SteamAchievement");
            migrationBuilder.CreateTable(
                name: "PlayerStats",
                columns: table => new
                {
                    gameName = table.Column<string>(nullable: false),
                    steamID = table.Column<long>(nullable: false),
                    success = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerStats", x => new { x.gameName, x.steamID });
                    table.ForeignKey(
                        name: "FK_PlayerStats_SteamPlayer_steamID",
                        column: x => x.steamID,
                        principalTable: "SteamPlayer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "SteamPlayerAchievements",
                columns: table => new
                {
                    apiname = table.Column<string>(nullable: false),
                    PlayerStatsgameName = table.Column<string>(nullable: true),
                    PlayerStatssteamID = table.Column<long>(nullable: true),
                    achieved = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SteamPlayerAchievements", x => x.apiname);
                    table.ForeignKey(
                        name: "FK_SteamPlayerAchievements_PlayerStats_PlayerStatsgameName_PlayerStatssteamID",
                        columns: x => new { x.PlayerStatsgameName, x.PlayerStatssteamID },
                        principalTable: "PlayerStats",
                        principalColumns: new[] { "gameName", "steamID" },
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_PlayersGameModel_SteamPlayer_playerId",
                table: "PlayersGameModel",
                column: "playerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId",
                table: "SteamPlayerFriend",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_PlayersGameModel_SteamPlayer_playerId", table: "PlayersGameModel");
            migrationBuilder.DropForeignKey(name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId", table: "SteamPlayerFriend");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropTable("SteamPlayerAchievements");
            migrationBuilder.DropTable("PlayerStats");
            migrationBuilder.AddColumn<long>(
                name: "SteamPlayerId",
                table: "SteamAchievement",
                nullable: false,
                defaultValue: 0L);
            migrationBuilder.AddForeignKey(
                name: "FK_PlayersGameModel_SteamPlayer_playerId",
                table: "PlayersGameModel",
                column: "playerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamAchievement_SteamPlayer_SteamPlayerId",
                table: "SteamAchievement",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SteamPlayerFriend_SteamPlayer_SteamPlayerId",
                table: "SteamPlayerFriend",
                column: "SteamPlayerId",
                principalTable: "SteamPlayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
