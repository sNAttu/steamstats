﻿using System;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Mvc;
using HoyryStats.Controllers;
using HoyryStats.Models;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Microsoft.AspNet.Authorization;
using System.Text.RegularExpressions;

namespace HoyryStats.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        private readonly ILogger<HomeController> _logger;
        public HomeController(ApplicationDbContext context, ILogger<HomeController> logger)
        {
            _context = context;
            _logger = logger;

        }
        [Authorize]
        public IActionResult UpdateDB()
        {
            UpdateDataToDb();
            return View();
        }

        public IActionResult Index()
        {
            if (User.IsSignedIn())
            {
                return View();
            }
            else
            {
                return RedirectToAction("About", "Home");
            }
            
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        #region Helper Methods
        public void UpdateDataToDb()
        {
            try
            {
                int appid = 252950;
                Regex linkParser = new Regex(@"\b(?:https?://|www\.)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                List<SteamNewsItem> resultNews = new List<SteamNewsItem>();
                List<SteamAchievement> resultAchievements = new List<SteamAchievement>();
                using (var client = new HttpClient())
                {
                    var newsUri = new Uri("http://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid="+ appid + "&count=5&maxlength=300&format=json");
                    var newsResponse = client.GetAsync(newsUri).Result;
                    var newsTextResult = newsResponse.Content.ReadAsStringAsync().Result;
                    RootObject newsRoot = JsonConvert.DeserializeObject<RootObject>(newsTextResult);
                    resultNews = (from item in newsRoot.appnews.newsitems
                                  select item).ToList();

                    var achiUri = new Uri("http://api.steampowered.com/ISteamUserStats/GetGlobalAchievementPercentagesForApp/v0002/?gameid=" + appid + "&format=json");
                    var achiResponse =  client.GetAsync(achiUri).Result;
                    var achiResult =  achiResponse.Content.ReadAsStringAsync().Result;
                    AchievementRoot achiRoot = JsonConvert.DeserializeObject<AchievementRoot>(achiResult);
                    resultAchievements = (from a in achiRoot.achievementpercentages.achievements
                                          select a).ToList();

                }

                using(var context = _context)
                {
                    var itemsInDb = (from n in context.SteamNewsItem
                                     select n.ID).ToList();

                    var achiesInDb = (from a in context.SteamAchievement
                                      select a.name).ToList();

                    foreach (var news in resultNews)
                    {
                        if (itemsInDb.Contains(news.ID))
                        {
                            _logger.LogInformation("News already in Database");
                        }
                        else
                        {

                            
                            string rawString = news.contents;
                            foreach (Match m in linkParser.Matches(rawString))
                            {
                                if (m.Value.Contains(".jpg") || m.Value.Contains(".png"))
                                {
                                    news.ImageUrl = m.Value;
                                    news.contents = news.contents.Replace(m.Value, "");
                                }
                                else
                                {
                                    news.ImageUrl = "";
                                }
                            }

                            news.lastUpdated = DateTime.Now;
                            _context.SteamNewsItem.Add(news);
                        }
                    }

                    foreach(var achi in resultAchievements)
                    {
                        if (achiesInDb.Contains(achi.name))
                        {
                            _logger.LogInformation("Achievement already in Database");
                        }
                        else
                        {
                            achi.lastUpdated = DateTime.Now;
                            _context.SteamAchievement.Add(achi);
                        }
                    }

                    context.SaveChanges();
                }

            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message + " " + ex.InnerException);
            }
        }
        #endregion
    }
}
