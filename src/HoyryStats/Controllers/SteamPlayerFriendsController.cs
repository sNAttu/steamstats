using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using HoyryStats.Models;

namespace HoyryStats.Controllers
{
    public class SteamPlayerFriendsController : Controller
    {
        private ApplicationDbContext _context;

        public SteamPlayerFriendsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: SteamPlayerFriends
        public IActionResult Index()
        {
            return View(_context.SteamPlayerFriend.ToList());
        }

        // GET: SteamPlayerFriends/Details/5
        public IActionResult Details(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamPlayerFriend steamPlayerFriend = _context.SteamPlayerFriend.Single(m => m.Id == id);
            if (steamPlayerFriend == null)
            {
                return HttpNotFound();
            }

            return View(steamPlayerFriend);
        }

        // GET: SteamPlayerFriends/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SteamPlayerFriends/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SteamPlayerFriend steamPlayerFriend)
        {
            if (ModelState.IsValid)
            {
                _context.SteamPlayerFriend.Add(steamPlayerFriend);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamPlayerFriend);
        }

        // GET: SteamPlayerFriends/Edit/5
        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamPlayerFriend steamPlayerFriend = _context.SteamPlayerFriend.Single(m => m.Id == id);
            if (steamPlayerFriend == null)
            {
                return HttpNotFound();
            }
            return View(steamPlayerFriend);
        }

        // POST: SteamPlayerFriends/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(SteamPlayerFriend steamPlayerFriend)
        {
            if (ModelState.IsValid)
            {
                _context.Update(steamPlayerFriend);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamPlayerFriend);
        }

        // GET: SteamPlayerFriends/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamPlayerFriend steamPlayerFriend = _context.SteamPlayerFriend.Single(m => m.Id == id);
            if (steamPlayerFriend == null)
            {
                return HttpNotFound();
            }

            return View(steamPlayerFriend);
        }

        // POST: SteamPlayerFriends/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(long id)
        {
            SteamPlayerFriend steamPlayerFriend = _context.SteamPlayerFriend.Single(m => m.Id == id);
            _context.SteamPlayerFriend.Remove(steamPlayerFriend);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
