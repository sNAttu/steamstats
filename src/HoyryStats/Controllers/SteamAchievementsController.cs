using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using HoyryStats.Models;

namespace HoyryStats.Controllers
{
    public class SteamAchievementsController : Controller
    {
        private ApplicationDbContext _context;

        public SteamAchievementsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: SteamAchievements
        public IActionResult Index()
        {
            return View(_context.SteamAchievement.ToList());
        }

        // GET: SteamAchievements/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamAchievement steamAchievement = _context.SteamAchievement.Single(m => m.ID == id);
            if (steamAchievement == null)
            {
                return HttpNotFound();
            }

            return View(steamAchievement);
        }

        // GET: SteamAchievements/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SteamAchievements/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SteamAchievement steamAchievement)
        {
            if (ModelState.IsValid)
            {
                _context.SteamAchievement.Add(steamAchievement);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamAchievement);
        }

        // GET: SteamAchievements/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamAchievement steamAchievement = _context.SteamAchievement.Single(m => m.ID == id);
            if (steamAchievement == null)
            {
                return HttpNotFound();
            }
            return View(steamAchievement);
        }

        // POST: SteamAchievements/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(SteamAchievement steamAchievement)
        {
            if (ModelState.IsValid)
            {
                _context.Update(steamAchievement);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamAchievement);
        }

        // GET: SteamAchievements/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamAchievement steamAchievement = _context.SteamAchievement.Single(m => m.ID == id);
            if (steamAchievement == null)
            {
                return HttpNotFound();
            }

            return View(steamAchievement);
        }

        // POST: SteamAchievements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            SteamAchievement steamAchievement = _context.SteamAchievement.Single(m => m.ID == id);
            _context.SteamAchievement.Remove(steamAchievement);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
