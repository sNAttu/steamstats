using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using HoyryStats.Models;

namespace HoyryStats.Controllers
{
    public class SteamGamesController : Controller
    {
        private ApplicationDbContext _context;

        public SteamGamesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: SteamGames
        public IActionResult Index()
        {
            return View(_context.SteamGame.ToList());
        }

        // GET: SteamGames/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamGame steamGame = _context.SteamGame.Single(m => m.Id == id);
            if (steamGame == null)
            {
                return HttpNotFound();
            }

            return View(steamGame);
        }

        // GET: SteamGames/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SteamGames/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SteamGame steamGame)
        {
            if (ModelState.IsValid)
            {
                _context.SteamGame.Add(steamGame);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamGame);
        }

        // GET: SteamGames/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamGame steamGame = _context.SteamGame.Single(m => m.Id == id);
            if (steamGame == null)
            {
                return HttpNotFound();
            }
            return View(steamGame);
        }

        // POST: SteamGames/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(SteamGame steamGame)
        {
            if (ModelState.IsValid)
            {
                _context.Update(steamGame);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamGame);
        }

        // GET: SteamGames/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamGame steamGame = _context.SteamGame.Single(m => m.Id == id);
            if (steamGame == null)
            {
                return HttpNotFound();
            }

            return View(steamGame);
        }

        // POST: SteamGames/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            SteamGame steamGame = _context.SteamGame.Single(m => m.Id == id);
            _context.SteamGame.Remove(steamGame);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        #region Helper methods

        #endregion

    }
}
