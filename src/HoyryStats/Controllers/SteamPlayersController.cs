using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using HoyryStats.Models;
using System.Net.Http;
using System;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace HoyryStats.Controllers
{
    public class SteamPlayersController : Controller
    {
        private ApplicationDbContext _context;
        private readonly ILogger<SteamPlayersController> _logger;

        public SteamPlayersController(ApplicationDbContext context, ILogger<SteamPlayersController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: SteamPlayers
        public IActionResult Index()
        {
            _logger.LogInformation(_context.SteamPlayer.First().Id.ToString());
            return View(_context.SteamPlayer.ToList());
        }

        // GET: SteamPlayers/Details/5
        public IActionResult Details(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamPlayer steamPlayer = _context.SteamPlayer.Single(m => m.Id == id);
            if (steamPlayer == null)
            {
                return HttpNotFound();
            }

            return View(steamPlayer);
        }

        // GET: SteamPlayers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SteamPlayers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SteamPlayer steamPlayer)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    SteamPlayer player = GetPlayerData(Constants.ApiKey, steamPlayer.Id);
                    player.playerGames = GetPlayerGames(Constants.ApiKey, steamPlayer.Id);
                    player.playerFriends = GetPlayerFriends(Constants.ApiKey, steamPlayer.Id);
                    foreach (var game in player.playerGames)
                    {
                        player.playerStats = GetPlayerAchievements(Constants.ApiKey, steamPlayer.Id, game.game);
                        UpdatePlayersGamesToDatabase(Constants.ApiKey, game.game, steamPlayer.Id);
                    }
                    _context.SteamPlayer.Add(player);
                    _context.SaveChanges();

                }
                catch(Exception ex)
                {
                    _logger.LogError(ex.Message + " " + ex.InnerException);
                }
                return RedirectToAction("Index");
            }
            return View(steamPlayer);
        }





        // GET: SteamPlayers/Edit/5
        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamPlayer steamPlayer = _context.SteamPlayer.Single(m => m.Id == id);
            if (steamPlayer == null)
            {
                return HttpNotFound();
            }
            return View(steamPlayer);
        }

        // POST: SteamPlayers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(SteamPlayer steamPlayer)
        {
            if (ModelState.IsValid)
            {
                _context.Update(steamPlayer);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamPlayer);
        }

        // GET: SteamPlayers/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamPlayer steamPlayer = _context.SteamPlayer.Single(m => m.Id == id);
            if (steamPlayer == null)
            {
                return HttpNotFound();
            }

            return View(steamPlayer);
        }

        // POST: SteamPlayers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(long id)
        {
            SteamPlayer steamPlayer = _context.SteamPlayer.Single(m => m.Id == id);
            _context.SteamPlayer.Remove(steamPlayer);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        #region helper methods
        private SteamPlayer GetPlayerData(string apikey, long id)
        {
            using (var client = new HttpClient())
            {
                var uri = new Uri("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" + Constants.ApiKey + "&steamids=" + id);
                var response = client.GetAsync(uri).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                PlayerRoot playerRoot = JsonConvert.DeserializeObject<PlayerRoot>(result);
                SteamPlayer player = playerRoot.response.players.SingleOrDefault();
                return player;
            }
        }

        private List<PlayersGameModel> GetPlayerGames(string apikey, long id)
        {
            List<PlayersGameModel> games = new List<PlayersGameModel>();
            using (var client = new HttpClient())
            {
                var uri = new Uri("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key="+Constants.ApiKey+"&steamid="+id+"&format=json");
                var response = client.GetAsync(uri).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                PlayerGameRoot gameRoot = JsonConvert.DeserializeObject<PlayerGameRoot>(result);
                games = gameRoot.response.games;

            }
            return games;
        }

        private List<SteamPlayerFriend> GetPlayerFriends(string apikey, long id)
        {
            List<SteamPlayerFriend> friends = new List<SteamPlayerFriend>();
            using (var client = new HttpClient())
            {
                var uri = new Uri("http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key="+apikey+"&steamid="+id+"&relationship=friend");
                var response = client.GetAsync(uri).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                FriendRoot friendsRoot = JsonConvert.DeserializeObject<FriendRoot>(result);
                friends = friendsRoot.friendslist.friends;
                return friends;
            }
        }

        private PlayerStats GetPlayerAchievements(string apikey, long id, int appid)
        {
            using (var client = new HttpClient())
            {
                var uri = new Uri("http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid="+appid+"&key="+ apikey + "&steamid="+id);
                var response = client.GetAsync(uri).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                PlayerAchievementRoot achievementRoot = JsonConvert.DeserializeObject<PlayerAchievementRoot>(result);
                return achievementRoot.playerstats;
            }
        }

        private void UpdatePlayersGamesToDatabase(string apikey, int appid, long playerId)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var uri = new Uri("http://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?key="+ apikey + "&appid=" + appid);
                    var response = client.GetAsync(uri).Result;
                    if (response.Content != null)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        if (!string.IsNullOrEmpty(result))
                        {
                            SteamGameRoot achievementRoot = JsonConvert.DeserializeObject<SteamGameRoot>(result);
                            if (achievementRoot != null && achievementRoot.game != null)
                            {
                                AvailableGameStats tempStat = new AvailableGameStats();
                                SteamGame tempGame = new SteamGame();
                                List<SteamGameAchievement> tempAchis = new List<SteamGameAchievement>();
                                List<SteamGameDefaultStats> tempStats = new List<SteamGameDefaultStats>();

                                if (
                                    achievementRoot.game.availableGameStats != null
                                    && achievementRoot.game.availableGameStats.achievements != null
                                    && achievementRoot.game.availableGameStats.stats != null
                                    && achievementRoot.game.availableGameStats.achievements.Count != 0
                                    && achievementRoot.game.availableGameStats.stats.Count != 0)
                                {
                                    tempGame = achievementRoot.game;
                                    tempStat = achievementRoot.game.availableGameStats;
                                    foreach (var achi in achievementRoot.game.availableGameStats.achievements)
                                    {
                                        if (achi.displayName != null && achi.description != null)
                                        {
                                            tempAchis.Add(achi);
                                        }
                                    }
                                    foreach (var stat in achievementRoot.game.availableGameStats.stats)
                                    {
                                        if (stat.displayname != null && stat.name != null)
                                        {
                                            tempStats.Add(stat);
                                        }
                                    }
                                    try
                                    {
                                        tempStat.achievements = tempAchis;
                                        tempStat.stats = tempStats;
                                        tempGame.availableGameStats = tempStat;
                                        _context.SteamGame.Add(tempGame);
                                        _context.SaveChanges();
                                    }
                                    catch (NullReferenceException ex)
                                    {
                                        _logger.LogWarning("Values missing in: " + tempGame.gameName);
                                        _logger.LogWarning(ex.Message);
                                    }
                                    catch (Exception ex)
                                    {
                                        throw ex;
                                    }
                                }

                            }
                        }
                    }
                    
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex.Message);
                    throw ex;
                }
            }
        }

        #endregion

    }
}
