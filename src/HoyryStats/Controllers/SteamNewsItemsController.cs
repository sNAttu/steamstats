using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using HoyryStats.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HoyryStats.Controllers
{
    public class SteamNewsItemsController : Controller
    {
        private ApplicationDbContext _context;

        public SteamNewsItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SteamNewsItems
        public IActionResult Index()
        {
            return View(_context.SteamNewsItem.ToList());
        }

        // GET: SteamNewsItems/Details/5
        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamNewsItem steamNewsItem = _context.SteamNewsItem.Single(m => m.ID == id);
            if (steamNewsItem == null)
            {
                return HttpNotFound();
            }

            return View(steamNewsItem);
        }

        // GET: SteamNewsItems/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SteamNewsItems/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SteamNewsItem steamNewsItem)
        {
            if (ModelState.IsValid)
            {
                _context.SteamNewsItem.Add(steamNewsItem);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamNewsItem);
        }

        // GET: SteamNewsItems/Edit/5
        public IActionResult Edit(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamNewsItem steamNewsItem = _context.SteamNewsItem.Single(m => m.ID == id);
            if (steamNewsItem == null)
            {
                return HttpNotFound();
            }
            return View(steamNewsItem);
        }

        // POST: SteamNewsItems/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(SteamNewsItem steamNewsItem)
        {
            if (ModelState.IsValid)
            {
                _context.Update(steamNewsItem);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(steamNewsItem);
        }

        // GET: SteamNewsItems/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SteamNewsItem steamNewsItem = _context.SteamNewsItem.Single(m => m.ID == id);
            if (steamNewsItem == null)
            {
                return HttpNotFound();
            }

            return View(steamNewsItem);
        }

        // POST: SteamNewsItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(string id)
        {
            SteamNewsItem steamNewsItem = _context.SteamNewsItem.Single(m => m.ID == id);
            _context.SteamNewsItem.Remove(steamNewsItem);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
        #region Helper Methods


        #endregion
    }
}
