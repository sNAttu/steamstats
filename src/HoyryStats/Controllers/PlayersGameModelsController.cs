using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using HoyryStats.Models;

namespace HoyryStats.Controllers
{
    public class PlayersGameModelsController : Controller
    {
        private ApplicationDbContext _context;

        public PlayersGameModelsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: PlayersGameModels
        public IActionResult Index()
        {
            return View(_context.PlayersGameModel.ToList());
        }

        // GET: PlayersGameModels/Details/5
        public IActionResult Details(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            PlayersGameModel playersGameModel = _context.PlayersGameModel.Single(m => m.playerId == id);
            if (playersGameModel == null)
            {
                return HttpNotFound();
            }

            return View(playersGameModel);
        }

        // GET: PlayersGameModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlayersGameModels/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PlayersGameModel playersGameModel)
        {
            if (ModelState.IsValid)
            {
                _context.PlayersGameModel.Add(playersGameModel);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playersGameModel);
        }

        // GET: PlayersGameModels/Edit/5
        public IActionResult Edit(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            PlayersGameModel playersGameModel = _context.PlayersGameModel.Single(m => m.playerId == id);
            if (playersGameModel == null)
            {
                return HttpNotFound();
            }
            return View(playersGameModel);
        }

        // POST: PlayersGameModels/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(PlayersGameModel playersGameModel)
        {
            if (ModelState.IsValid)
            {
                _context.Update(playersGameModel);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playersGameModel);
        }

        // GET: PlayersGameModels/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(long? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            PlayersGameModel playersGameModel = _context.PlayersGameModel.Single(m => m.playerId == id);
            if (playersGameModel == null)
            {
                return HttpNotFound();
            }

            return View(playersGameModel);
        }

        // POST: PlayersGameModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(long id)
        {
            PlayersGameModel playersGameModel = _context.PlayersGameModel.Single(m => m.playerId == id);
            _context.PlayersGameModel.Remove(playersGameModel);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
