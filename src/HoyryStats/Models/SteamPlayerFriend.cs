﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HoyryStats.Models
{
    public class FriendRoot
    {
        [JsonProperty]
        public Friends friendslist { get; set; }
    }
    public class Friends
    {
        [JsonProperty]
        public List<SteamPlayerFriend> friends { get; set; }
    }
    public class SteamPlayerFriend
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty("steamid")]
        public long Id { get; set; }
        [JsonProperty]
        public string relationship { get; set; }
        [JsonProperty("friends_since")]
        public string friendsSince { get; set; }
        public long SteamPlayerId { get; set; }
        [ForeignKey("SteamPlayerId")]
        public SteamPlayer player { get; set; }
    }
}
