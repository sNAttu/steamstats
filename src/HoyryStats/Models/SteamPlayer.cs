﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Linq;
using System.Threading.Tasks;

namespace HoyryStats.Models
{
    public class PlayerRoot
    {
        [JsonProperty]
        public PlayerResponse response { get; set; }
    }

    public class PlayerResponse
    {
        [JsonProperty]
        public List<SteamPlayer> players { get; set; }
    }

    public class SteamPlayer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty("steamid")]
        public long Id { get; set; }
        [JsonProperty]
        public int communityvisibilitystate { get; set; }
        [JsonProperty]
        public int profilestate { get; set; }
        [JsonProperty]
        public string lastlogoff { get; set; }
        [JsonProperty]
        public string profileurl { get; set; }
        [JsonProperty]
        public string avatar { get; set; }
        [JsonProperty]
        public string avatarmedium { get; set; }
        [JsonProperty]
        public string avatarfull { get; set; }
        [JsonProperty]
        public int personastate { get; set; }
        [JsonProperty]
        public string realname { get; set; }
        [JsonProperty]
        public string primaryclanid { get; set; }
        [JsonProperty]
        public string timecreated { get; set; }
        [JsonProperty]
        public int personastateflags { get; set; }
        [JsonProperty]
        public string loccountrycode { get; set; }
        [JsonProperty]
        public string locstatecode { get; set; }
        [JsonProperty]
        public int loccityid { get; set; }
        public int gameCount { get; set; }
        public List<PlayersGameModel> playerGames { get; set; }
        public PlayerStats playerStats { get; set; }
        public List<SteamPlayerFriend> playerFriends { get; set; }
        public List<SteamPlayerOrganization> Organizations { get; set; }
    }
}
