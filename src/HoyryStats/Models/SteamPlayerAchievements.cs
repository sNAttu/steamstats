﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HoyryStats.Models
{
    public class PlayerAchievementRoot
    {
        public PlayerStats playerstats { get; set; }
    }

    public class PlayerStats
    {
        [JsonProperty]
        public long steamID { get; set; }
        [JsonProperty]
        public string gameName { get; set; }
        [JsonProperty]
        public List<SteamPlayerAchievements> achievements { get; set; }
        [JsonProperty]
        public bool success { get; set; }
        [ForeignKey("steamID")]
        public SteamPlayer player { get; set; }
    }

    public class SteamPlayerAchievements
    {
        [Key]
        [JsonProperty]
        public string apiname { get; set; }
        [JsonProperty]
        public int achieved { get; set; }

    }
}
