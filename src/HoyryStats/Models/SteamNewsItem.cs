﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace HoyryStats.Models
{
    public class SteamNewsItem
    {   
        [JsonProperty("gid")]
        public string ID { get; set; }
        [JsonProperty]
        public string title { get; set; }
        [JsonProperty]
        public string url { get; set; }
        [JsonProperty("is_external_url")]
        public bool isExternalUrl { get; set; }
        [JsonProperty]
        public string author { get; set; }
        [JsonProperty]
        public string contents { get; set; }
        [JsonProperty("feedlabel")]
        public string feedLabel { get; set; }
        [JsonProperty]
        public string date { get; set; }
        [JsonProperty]
        public string feedname { get; set; }
        public DateTime lastUpdated { get; set; }
        public string ImageUrl { get; set; }
    }
    public class RootObject
    {
        [JsonProperty]
        public SteamNews appnews { get; set; }
    }

    public class SteamNews
    {
        [JsonProperty]
        public int appid { get; set; }
        [JsonProperty]
        public List<SteamNewsItem> newsitems { get; set; }
    }

}
