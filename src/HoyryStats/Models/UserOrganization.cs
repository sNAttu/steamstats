﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoyryStats.Models
{
    public class Organization
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool isActive { get; set; }
        public List<SteamPlayerOrganization> OrganizationUsers { get; set; }
    }

    public class SteamPlayerOrganization
    {
        public int SteamPlayerId { get; set; }
        public SteamPlayer SteamPlayer { get; set; }

        public int OrganizationId { get; set; }
        public Organization organization { get; set; }
    }

}
