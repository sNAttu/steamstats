﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using HoyryStats.Models;

namespace HoyryStats.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<PlayersGameModel>().HasKey(k => new { k.playerId, k.game });
            builder.Entity<PlayerStats>().HasKey(k => new { k.gameName, k.steamID });
            builder.Entity<SteamPlayerOrganization>().HasKey(k => new { k.OrganizationId, k.SteamPlayerId });
        }
        public DbSet<SteamNewsItem> SteamNewsItem { get; set; }
        public DbSet<SteamAchievement> SteamAchievement { get; set; }
        public DbSet<SteamPlayer> SteamPlayer { get; set; }
        public DbSet<SteamPlayerFriend> SteamPlayerFriend { get; set; }
        public DbSet<PlayersGameModel> PlayersGameModel { get; set; }
        public DbSet<SteamGame> SteamGame { get; set; }
    }
}
