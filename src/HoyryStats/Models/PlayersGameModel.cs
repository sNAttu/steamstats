﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HoyryStats.Models
{
    public class PlayerGameRoot
    {
        [JsonProperty]
        public PlayerGameResponse response { get; set; }
    }

    public class PlayerGameResponse
    {
        [JsonProperty("game_count")]
        public int gameCount { get; set; }
        [JsonProperty]
        public List<PlayersGameModel> games { get; set; }
    }

    public class PlayersGameModel
    {
        public long playerId { get; set; }
        [JsonProperty("appid")]
        public int game { get; set; }
        [JsonProperty("playtime_forever")]
        public int playTime { get; set; }
        [ForeignKey("playerId")]
        public SteamPlayer owner { get; set; }
    }
}
