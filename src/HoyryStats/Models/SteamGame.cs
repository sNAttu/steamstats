﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HoyryStats.Models
{
    public class SteamGameRoot
    {
        [JsonProperty]
        public SteamGame game { get; set; }
    }
    public class SteamGame
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [JsonProperty]
        public string gameName { get; set; }
        [JsonProperty]
        public string gameVersion { get; set; }
        [JsonProperty]
        public AvailableGameStats availableGameStats { get; set; }
        public long steamGameId { get; set; }

    }
    public class AvailableGameStats
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [JsonProperty]
        public List<SteamGameAchievement> achievements { get; set; }
        [JsonProperty]
        public List<SteamGameDefaultStats> stats { get; set; }
        public int relatedGameId { get; set; }
        [ForeignKey("relatedGameId")]
        public SteamGame relatedGame { get; set; }
    }
    public class SteamGameAchievement
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [JsonProperty]
        public string name { get; set; }
        [JsonProperty]
        public int defaultvalue { get; set; }
        [JsonProperty]
        public string displayName { get; set; }
        [JsonProperty]
        public int hidden { get; set; }
        [JsonProperty]
        public string description { get; set; }
        [JsonProperty]
        public string icon { get; set; }
        [JsonProperty]
        public string icongray { get; set; }
        public int statId { get; set; }
        [ForeignKey("statId")]
        public AvailableGameStats stats { get; set; }
    }

    public class SteamGameDefaultStats
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [JsonProperty]
        public string name { get; set; }
        [JsonProperty]
        public long defaultvalue { get; set; }
        [JsonProperty]
        public string displayname { get; set; }
        public int statId { get; set; }
        [ForeignKey("statId")]
        public AvailableGameStats stats { get; set; }
    }

}
