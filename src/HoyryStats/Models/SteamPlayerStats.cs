﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoyryStats.Models
{
    public class SteamPlayerRoot
    {
        [JsonProperty]
        public SteamPlayerStatistic playerstats { get; set; }
    }
    public class SteamPlayerStatistic
    {
        [JsonProperty("steamid")]
        public long Id { get; set; }
        [JsonProperty]
        public string gameName { get; set; }
        [JsonProperty]
        public List<SteamPlayerStats> achievements { get; set; }
        [JsonProperty]
        public List<SteamPlayerStatAchievements> stats { get; set; }
    }

    public class SteamPlayerStatAchievements
    {
        [JsonProperty]
        public string name { get; set; }
        [JsonProperty]
        public int achieved { get; set; }
    }

    public class SteamPlayerStats
    {
        [JsonProperty]
        public string name { get; set; }
        [JsonProperty]
        public int value { get; set; }
    }
}
