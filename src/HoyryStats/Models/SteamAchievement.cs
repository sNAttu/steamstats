﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HoyryStats.Models
{
    public class AchievementRoot
    {
        [JsonProperty]
        public AchievementPercentages achievementpercentages { get; set; }
    }

    public class AchievementPercentages
    {
        [JsonProperty]
        public List<SteamAchievement> achievements { get; set; }
    }

    public class SteamAchievement
    {
        public int ID { get; set; }
        [JsonProperty]
        public string name { get; set; }
        [JsonProperty]
        public decimal percent { get; set; }
        public DateTime lastUpdated { get; set; }
        public int game { get; set; }

    }

}
