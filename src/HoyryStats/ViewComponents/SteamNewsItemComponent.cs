﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoyryStats.Controllers;
using HoyryStats.Models;

namespace HoyryStats.ViewComponents
{
    [ViewComponent(Name = "News")]
    public class SteamNewsItemComponent : ViewComponent
    {

        private readonly ApplicationDbContext db;

        public SteamNewsItemComponent(ApplicationDbContext context)
        {
            db = context;
        }

        public IViewComponentResult Invoke()
        {
            return View(db.SteamNewsItem.ToList());
        }
    }

}
